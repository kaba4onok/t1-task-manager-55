package ru.t1.rleonov.tm.command.data;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.rleonov.tm.command.AbstractCommand;
import ru.t1.rleonov.tm.enumerated.Role;

@Getter
@Setter
@Component
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected IDomainEndpoint domainEndpoint;

    @Nullable
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
