package ru.t1.rleonov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.dto.request.ServerSaveDataJsonJaxBRequest;

@Component
public final class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-save-json-jaxb";

    @NotNull
    private static final String DESCRIPTION = "Save data in json file using jaxb.";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final ServerSaveDataJsonJaxBRequest request = new ServerSaveDataJsonJaxBRequest(getToken());
        getDomainEndpoint().saveDataJsonJaxB(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
