package ru.t1.rleonov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.api.model.ICommand;
import ru.t1.rleonov.tm.command.AbstractCommand;
import java.util.Collection;

@Component
public final class ArgumentListCommand extends AbstractSystemCommand {

    @NotNull
    private static final String ARGUMENT = "-arg";

    @NotNull
    private static final String NAME = "arguments";

    @NotNull
    private static final String DESCRIPTION = "Show application arguments.";

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@NotNull final ICommand command : commands) {
            @Nullable final String name = command.getArgument();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
