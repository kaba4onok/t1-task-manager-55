package ru.t1.rleonov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ServerLoadDataBackupRequest extends AbstractUserRequest {

    public ServerLoadDataBackupRequest(@Nullable String token) {
        super(token);
    }

}
