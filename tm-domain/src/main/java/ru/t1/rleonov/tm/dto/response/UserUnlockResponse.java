package ru.t1.rleonov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public final class UserUnlockResponse extends AbstractUserResponse {

    public UserUnlockResponse(@Nullable UserDTO user) {
        super(user);
    }

}
