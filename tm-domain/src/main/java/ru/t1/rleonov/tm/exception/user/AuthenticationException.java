package ru.t1.rleonov.tm.exception.user;

public final class AuthenticationException extends AbstractUserException {

    public AuthenticationException() {
        super("Authentication Error!!!");
    }

}
