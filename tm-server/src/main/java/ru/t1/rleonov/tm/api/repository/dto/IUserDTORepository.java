package ru.t1.rleonov.tm.api.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.model.UserDTO;
import java.util.List;

public interface IUserDTORepository extends IDTORepository<UserDTO> {

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    @SneakyThrows
    UserDTO findOneById(@Nullable String id);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    Boolean isLoginExist(@Nullable String login);

    Boolean isEmailExist(@Nullable String email);

    void clear();

}
