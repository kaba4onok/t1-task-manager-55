package ru.t1.rleonov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.model.IUserRepository;
import ru.t1.rleonov.tm.model.User;

public interface IUserService extends IService<User>, IUserRepository {

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull
    User removeByLogin(@Nullable String login);

    @NotNull
    User removeByEmail(@Nullable String email);

    @NotNull
    User setPassword(
            @Nullable String id,
            @Nullable String password
    );

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    @NotNull
    User lockUserByLogin(@Nullable String login);

    @NotNull
    User unlockUserByLogin(@Nullable String login);

}
