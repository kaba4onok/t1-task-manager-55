package ru.t1.rleonov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.model.IProjectRepository;
import ru.t1.rleonov.tm.model.Project;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        @NotNull final String jpql = "SELECT m FROM Project m";
        return entityManager.createQuery(jpql, Project.class).getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull String userId, @NotNull String sort) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.user.id = :userId ORDER BY :sort";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .setParameter("sort", sort)
                .getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty() || id.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.user.id = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM Project";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public void clear(@NotNull String userId) {
        if (userId.isEmpty()) return;
        @NotNull final String jpql = "DELETE FROM Project m WHERE m.user.id = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
